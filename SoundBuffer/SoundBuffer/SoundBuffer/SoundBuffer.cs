//This code is a c# porting, modified version of Shay Green Blipbuf library - LGPL License
using System;
using UnityEngine;

namespace NesEmu
{
    using buf_t = Int32;
    using fixed_t = UInt64;

    public class SoundBuffer : ISoundBuffer
    {
        private const int pre_shift = 32;
        private const int time_bits = pre_shift + 20;
        private static int max_sample = +32767;
        private static int min_sample = -32768;

        private static fixed_t time_unit = (fixed_t)1 << time_bits;

        private const int bass_shift = 9; /* affects high-pass filter breakpoint frequency */
        private const int end_frame_extra = 2;  /* allows deltas slightly after frame length */

        private const int half_width = 8;
        private const int phase_bits = 5;
        private const int phase_count = 1 << phase_bits;
        private const int delta_bits = 15;
        private const int delta_unit = 1 << delta_bits;
        private const int frac_bits = time_bits - pre_shift;

        private static fixed_t blip_max_ratio = 1 << 20;
        private static fixed_t blip_max_frame = 4000;

        public fixed_t factor;
        public fixed_t offset;
        public int start;
        public volatile int end;

        public int SamplesAvailable
        {
            get
            {
                //lock (this)
                //{
                //    return end >= start ? end - start : size + end - start;
                //}
                var result = end - start;
                if (result < 0)
                    result += size;
                return result;
            }
        }

        public int size;
        public int integrator;
        public buf_t[] samples;

        private void Next(ref int index)
        {
            index++;
            if (index == size)
                index = 0;
        }

        private void ClampSize(ref int index)
        {
            if (index >= size)
                index -= size;
        }

        private static int ArithShift(int n, int shift)
        {
            return n >> shift;
        }

        private static void Clamp(ref int n)
        {
            if ((short)n != n)
                n = ArithShift(n, 16) ^ max_sample;
        }

        private static void CheckAssumptions()
        {
            int n;

            Debug.Assert((-3 >> 1) == -2); /* right shift must preserve sign */

            n = max_sample * 2;
            Clamp(ref n);
            Debug.Assert(n == max_sample);

            n = min_sample * 2;
            Clamp(ref n);
            Debug.Assert(n == min_sample);

            Debug.Assert(blip_max_ratio <= time_unit);
            unchecked
            {
                Debug.Assert(blip_max_frame <= (fixed_t)(-1) >> time_bits);
            }
        }

        public SoundBuffer(int _size)
        {
            factor = time_unit / blip_max_ratio;
            size = _size;
            samples = new buf_t[size];
            Clear();
            CheckAssumptions();
        }

        public void SetRates(double clock_rate, double sample_rate)
        {
            double f = time_unit * sample_rate / clock_rate;
            factor = (fixed_t)Math.Ceiling(f);
            Clear();
        }

        public void Clear()
        {
            /* We could set offset to 0, factor/2, or factor-1. 0 is suitable if
            factor is rounded up. factor-1 is suitable if factor is rounded down.
            Since we don't know rounding direction, factor/2 accommodates either,
            with the slight loss of showing an error in half the time. Since for
            a 64-bit factor this is years, the halving isn't a problem. */

            offset = factor / 2;
            start = 0;
            end = 0;
            integrator = 0;
        }

        public int ClocksNeeded(int samples)
        {
            fixed_t needed;

            /* Fails if buffer can't hold that many more samples */
            Debug.Assert(samples >= 0 && SamplesAvailable + samples <= size);

            needed = (fixed_t)samples * time_unit;
            if (needed < offset)
                return 0;

            return (int)((needed - offset + factor - 1) / factor);
        }

        public void EndFrame(uint t)
        {
            //lock (this)
            {
                fixed_t off = t * factor + offset;
                offset = off & (time_unit - 1);
                var newEnd = end + (buf_t)(off >> time_bits);
                end = newEnd < size ? newEnd : newEnd - size;
            }
        }

        public int ReadSamples(float[] output, bool stereo)
        {
            int count = Mathf.Min(output.Length, SamplesAvailable);

            if (count > 0)
            {
                int index = 0;
                int step = stereo ? 2 : 1;
                int newStart = (start + count);
                ClampSize(ref newStart);
                int sum = integrator;

                do
                {
                    /* Eliminate fraction */
                    int s = ArithShift(sum, delta_bits);

                    sum += samples[start];
                    samples[start] = 0;
                    Next(ref start);
                    Clamp(ref s);
                    output[index] = s / (float)(max_sample);
                    index += step;

                    /* High-pass filter */
                    sum -= s << (delta_bits - bass_shift);
                } while (start != newStart);
                integrator = sum;
            }

            return count;
        }

        private static readonly int[] bl_step = new int[]        {
            43, -115,  350, -488, 1136, -914, 5861,21022,
            44, -118,  348, -473, 1076, -799, 5274,21001,
            45, -121,  344, -454, 1011, -677, 4706,20936,
            46, -122,  336, -431,  942, -549, 4156,20829,
            47, -123,  327, -404,  868, -418, 3629,20679,
            47, -122,  316, -375,  792, -285, 3124,20488,
            47, -120,  303, -344,  714, -151, 2644,20256,
            46, -117,  289, -310,  634,  -17, 2188,19985,
            46, -114,  273, -275,  553,  117, 1758,19675,
            44, -108,  255, -237,  471,  247, 1356,19327,
            43, -103,  237, -199,  390,  373,  981,18944,
            42,  -98,  218, -160,  310,  495,  633,18527,
            40,  -91,  198, -121,  231,  611,  314,18078,
            38,  -84,  178,  -81,  153,  722,   22,17599,
            36,  -76,  157,  -43,   80,  824, -241,17092,
            34,  -68,  135,   -3,    8,  919, -476,16558,
            32,  -61,  115,   34,  -60, 1006, -683,16001,
            29,  -52,   94,   70, -123, 1083, -862,15422,
            27,  -44,   73,  106, -184, 1152,-1015,14824,
            25,  -36,   53,  139, -239, 1211,-1142,14210,
            22,  -27,   34,  170, -290, 1261,-1244,13582,
            20,  -20,   16,  199, -335, 1301,-1322,12942,
            18,  -12,   -3,  226, -375, 1331,-1376,12293,
            15,   -4,  -19,  250, -410, 1351,-1408,11638,
            13,    3,  -35,  272, -439, 1361,-1419,10979,
            11,    9,  -49,  292, -464, 1362,-1410,10319,
            9,   16,  -63,  309, -483, 1354,-1383, 9660,
            7,   22,  -75,  322, -496, 1337,-1339, 9005,
            6,   26,  -85,  333, -504, 1312,-1280, 8355,
            4,   31,  -94,  341, -507, 1278,-1205, 7713,
            3,   35, -102,  347, -506, 1238,-1119, 7082,
            1,   40, -110,  350, -499, 1190,-1021, 6464,
            0,   43, -115,  350, -488, 1136, -914, 5861
        };

        /* Shifting by pre_shift allows calculation using unsigned int rather than
        possibly-wider fixed_t. On 32-bit platforms, this is likely more efficient.
        And by having pre_shift 32, a 32-bit platform can easily do the shift by
        simply ignoring the low half. */

        public void AddDelta(uint time, int delta)
        {
            //lock (this)
            {
                uint fixe = (uint)((time * factor + offset) >> pre_shift);
                int index = (int)(end + (fixe >> frac_bits));
                ClampSize(ref index);

                int phase_shift = frac_bits - phase_bits;
                int phase = (int)(fixe >> phase_shift & (phase_count - 1));

                int interp = (int)(fixe >> (phase_shift - delta_bits) & (delta_unit - 1));
                int delta2 = (delta * interp) >> delta_bits;
                delta -= delta2;

                int input1 = phase << 3;
                int input2 = (phase + 1) << 3;

                for (int i = 0; i < 8; i++)
                {
                    samples[index] += bl_step[input1 | i] * delta + bl_step[input2 | i] * delta2;
                    Next(ref index);
                }

                input1 = (phase_count - phase) << 3;
                input2 = (phase_count - phase - 1) << 3;
                for (int i = 7; i >= 0; i--)
                {
                    samples[index] += bl_step[input1 | i] * delta + bl_step[input2 | i] * delta2;
                    Next(ref index);
                }
            }
        }

        public void AddDeltaFast(uint time, int delta)
        {
            uint fixe = (uint)((time * factor + offset) >> pre_shift);
            int index = (int)(end + (fixe >> frac_bits));

            int interp = (int)(fixe >> (frac_bits - delta_bits) & (delta_unit - 1));
            int delta2 = delta * interp;

            samples[(index + 7) % size] += delta * delta_unit - delta2;
            samples[(index + 8) % size] += delta2;
        }

        public void AddDeltasFast(Delta[] delta, int count)
        {
            for (int i = 0; i < count; i++)
            {
                AddDeltaFast(delta[i].time, delta[i].value);
            }
        }

        public unsafe void AddDeltas(Delta* delta, int count)
        {
            for (int i = 0; i < count; i++)
            {
                AddDelta(delta->time, delta->value);
                delta++;
            }
        }

        public void AddDeltas(Delta[] deltas, int count)
        {
            for (int i = 0; i < count; i++)
                AddDelta(deltas[i].time, deltas[i].value);
        }
    }
}