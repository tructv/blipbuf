using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NesEmu
{
    public struct Delta
    {
        public uint time;
        public int value;
        public Delta(uint _time, int _value)
        {
            time = _time;
            value = _value;
        }
    }

    interface ISoundBuffer
    {

        void SetRates(double clock_rate, double sample_rate);

        void Clear();

        void AddDelta(uint clock_time, int delta);

        void AddDeltas(Delta[] deltas, int count);

        void AddDeltaFast(uint clock_time, int delta);

        int ClocksNeeded(int sample_count);

        void EndFrame(uint clock_duration);

        int SamplesAvailable { get; }

        int ReadSamples(float[] output, bool stereo);
    }
}
